import * as api from "$lib/api"
import type { PageServerLoad } from "./$types"

export const load: PageServerLoad = async ({ params }) => {
  const product = await api.getProductById(params.id)
  if (product) {
    return { product }
  }

}
