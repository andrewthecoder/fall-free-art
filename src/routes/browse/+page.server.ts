import * as api from "$lib/api"
import type { PageServerLoad } from "./$types"
import { error } from "@sveltejs/kit"

export const load: PageServerLoad = async () => {
  const products = await api.getAllProducts()

  if (products) {
    return { products }
  }

  throw error(404, "This doesn't seem to be a valid product!")
}
