// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare global {
	namespace App {
		// interface Error {}
		// interface Locals {}
		// interface PageData {}
		// interface Platform {}
		interface Option {
			id: string
			name: string
			price: number
			description: string
		}

		interface ProductOption {
			id: string
			name: string
			values: Option[]
		}

		interface Product {
			name: string
			options: ProductOption[]
			meta_description: string
			images: { file: { url: string } }[]
			id: string
		}
	}
}

export { };
