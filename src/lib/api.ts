import { SWELL_STOREID, SWELL_SECRET } from "$env/static/private"
import { error } from "@sveltejs/kit"

const storeid = SWELL_STOREID
const pvt = SWELL_SECRET
const baseurl = 'https://api.swell.store/products'
const headers = new Headers()
headers.append("Authorization", `Basic ${btoa(`${storeid}:${pvt}`)}`)

export const getAllProducts = async (): Promise<App.Product[] | null> => {
  try {
    const response = await fetch(baseurl, { headers: headers })

    if (!response.ok) {
      throw error(response.status, response.statusText)
    }

    const data = await response.json()

    const products: App.Product[] = data.results.map((prod: any) => ({
      name: prod.name,
      meta_description: prod.meta_description,
      images: prod.images,
      id: prod.id
    }))

    return products
  } catch (err) {
    if (err instanceof SyntaxError && err.message.includes("JSON")) {
      throw error(400, "BAD REQUEST")
    }

    throw err
  }
}

export const getProductById = async (prodid: string): Promise<App.Product | null> => {
  try {
    const response = await fetch(baseurl + '/' + prodid, { headers: headers })

    if (!response.ok) {
      throw error(response.status, response.statusText)
    }

    const data = await response.json()

    const product: App.Product = {
      name: data.name,
      options: data.options as App.ProductOption[],
      meta_description: data.meta_description,
      images: data.images,
      id: data.id
    }

    return product

  } catch (err) {
    if (err instanceof SyntaxError && err.message.includes("JSON")) {
      throw error(400, "BAD REQUEST")
    }

    throw err
  }
}
